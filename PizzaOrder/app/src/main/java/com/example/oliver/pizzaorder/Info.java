/**
 * Project: MAD A2 - Pizza Order
 * Authors: Kelson Conyard, Josh Medeiros, Oliver Sousa, Aric Vogel
 * Date: 2016-03-13
 * Description: Stores information about the user as well as the number of pizzas.
 */

package com.example.oliver.pizzaorder;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

/**
 * Handles information about the user and number of pizzas
 */
public class Info extends Activity {

    private static final String TAG = "Info";

    Button nxtButton;
    NumberPicker pizzaNumber;

    //used to package the values that are being passed between activities (name and pizza count)
    Bundle dataBundle = new Bundle();

    TextView fullName;
    TextView address;
    TextView phone;

    /**
     * Creates the Info activity.
     * @param savedInstanceState Any data supplied in onSaveInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        pizzaNumber = (NumberPicker)findViewById(R.id.PizzaNumber);
        nxtButton = (Button)findViewById(R.id.NextBtn);
        fullName = ((TextView)findViewById(R.id.NameEdit));
        address = (TextView)findViewById(R.id.AddressEdit);
        phone = (TextView)findViewById(R.id.PhoneEdit);

        pizzaNumber.setMinValue(1);
        pizzaNumber.setMaxValue(9);

        nxtButton.setOnClickListener(new View.OnClickListener(){
            @Override
                    public void onClick(View v){

                Log.i(TAG, "Name=" + fullName.getText().toString());
                Log.i(TAG, "Address=" + address.getText().toString());
                Log.i(TAG, "Phone=" + phone.getText().toString());
                Log.i(TAG, "Total Pizzas=" + pizzaNumber.getValue());

                //Check to see if the fields are empty before moving on to the next activity.
                if(fullName.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"You must enter a name" , Toast.LENGTH_SHORT).show();
                }
                else if(address.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"You must enter an address", Toast.LENGTH_SHORT).show();
                }
                else if(phone.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"You must enter a phone number", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    //send the data to the next activity
                    Intent intent = new Intent(getApplicationContext(), Toppings.class);

                    dataBundle.putInt("pizzaCount", pizzaNumber.getValue());
                    dataBundle.putString("name", fullName.getText().toString());
                    intent.putExtras(dataBundle);
                    startActivity(intent);
                }

            }
        });
    }
}
