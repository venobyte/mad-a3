/**
 * Project: MAD A3 - Pizza Order
 * Authors: Kelson Conyard, Josh Medeiros, Oliver Sousa, Aric Vogel
 * Date: 2016-04-16
 * Description: Adds widget functionality for the app to add to the home screen and shows
 *              the top 3 toppings for the pizza.
 */

package com.example.oliver.pizzaorder;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

/**
 * Implementation of App Widget functionality.
 */
public class pizzaWidget extends AppWidgetProvider {


    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int[] appWidgetId) {

        CharSequence widgetText = context.getString(R.string.appwidget_text);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.pizza_widget);


        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }


    /**
     * Update the widget to reflect changes
     * @param context The pizza toppings.
     * @param appWidgetManager the manager for the widgets
     * @param appWidgetIds the ids for the widgets
     * @return none
     */
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int i = 0; i < appWidgetIds.length; i++) {
            Intent intent = new Intent(context, Result.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.pizza_widget);
            views.setOnClickPendingIntent(R.id.pizzaWidgetList, pendingIntent);

            //get the top 3 toppings from the database
            DAL db = new DAL(context);
            String[] toppings = db.getTopToppings();

            views.setTextViewText(R.id.firstView, toppings[0]==null ? "" : toppings[0]);
            views.setTextViewText(R.id.secondView, toppings[1]==null ? "" : toppings[1]);
            views.setTextViewText(R.id.thirdView, toppings[2]==null ? "" : toppings[2]);

            //update the widget
            int appWidgetId = appWidgetIds[i];
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    /**
     * Reflects what happens when clicked on widget
     * @param context The pizza toppings.
     * @param intent the manager for the widgets
     * @return none
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        if (intent.getAction().equals(DAL.TASK_MODIFIED)) {
            AppWidgetManager manager =
                    AppWidgetManager.getInstance(context);
            ComponentName provider =
                    new ComponentName(context, pizzaWidget.class);
            int[] appWidgetIds = manager.getAppWidgetIds(provider);
            onUpdate(context, manager, appWidgetIds);
        }
    }

}

