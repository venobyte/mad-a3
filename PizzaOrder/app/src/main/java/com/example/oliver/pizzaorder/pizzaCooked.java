/**
 * Project: MAD A3 - Pizza Order
 * Authors: Kelson Conyard, Josh Medeiros, Oliver Sousa, Aric Vogel
 * Date: 2016-04-15
 * Description: Shows the pizza cooked
 */

package com.example.oliver.pizzaorder;

import android.os.Bundle;
import android.app.Activity;

/**
 * The pizza cooked activity
 */
public class pizzaCooked extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_cooked);
    }

}
