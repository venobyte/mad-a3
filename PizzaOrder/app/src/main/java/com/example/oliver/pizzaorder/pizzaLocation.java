/**
 * Project: MAD A3 - Pizza Order
 * Authors: Kelson Conyard, Josh Medeiros, Oliver Sousa, Aric Vogel
 * Date: 2016-04-15
 * Description: Shows the pizza location
 */

package com.example.oliver.pizzaorder;

import android.support.v4.app.FragmentActivity;
import android.app.Activity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Class for the pizza location
 */
public class pizzaLocation extends Activity {

    static final LatLng pizzaHutLoc = new LatLng(43.429846, -80.432244);
    private GoogleMap googleMap;

    /**
     * Create the activity
     * @param savedInstanceState The previously saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizza_location);

        try {
            if (googleMap == null) {
                googleMap = ((MapFragment) getFragmentManager().
                        findFragmentById(R.id.map)).getMap();
            }
            googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            Marker TP = googleMap.addMarker(new MarkerOptions().
                    position(pizzaHutLoc).title("Pizza Hut"));
            CameraUpdate center = CameraUpdateFactory.newLatLng(pizzaHutLoc);
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(15);

            googleMap.moveCamera(center);
            googleMap.animateCamera(zoom);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }


}
