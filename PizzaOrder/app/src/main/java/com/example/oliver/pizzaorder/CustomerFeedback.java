/**
 * Project: MAD A2 - Pizza Order
 * Authors: Kelson Conyard, Josh Medeiros, Oliver Sousa, Aric Vogel
 * Date: 2016-03-13
 * Description: Customer feedback screen to save a text file or open a the last text file that was saved.
 */


package com.example.oliver.pizzaorder;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.Manifest;

/**
 * Handles customer feedback.
 */
public class CustomerFeedback extends Activity {
    /**
     * Creates the CustomerFeedback activity.
     * @param savedInstanceState Any data supplied in onSaveInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_customerfeedback);

        //event handler to write the text from the user to a file.
        Button btnSubmit = (Button)findViewById(R.id.btnSendFeedback);
        btnSubmit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText feedback = (EditText)findViewById(R.id.txtCustomerFeedback);
                writeToFile(feedback.getText().toString());
            }
        });

        //event handler to get text from file and populate widget
        Button btnViewFeedback = (Button)findViewById(R.id.btnViewFeedback);
        btnViewFeedback.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String previousFeedback = readFromFile();
                EditText feedback = (EditText)findViewById(R.id.txtCustomerFeedback);
                feedback.setText(previousFeedback);
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && checkSelfPermission(android.Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.READ_CONTACTS}, 1);
        }


        ContentResolver resolver = getContentResolver();
        Cursor contacts = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);

        String[] from = new String[]{ContactsContract.Contacts.DISPLAY_NAME};
        int[] to = new int[]{R.id.text};

        SimpleCursorAdapter contactAdapter = new SimpleCursorAdapter(this, R.layout.row, contacts, from, to, 0);

        Spinner spinnerFriends = (Spinner)findViewById(R.id.spinnerFriends);
        spinnerFriends.setAdapter(contactAdapter);
    }

    /**
     * Write feedback to file.
     * @param data The feedback
     */
    private void writeToFile(String data) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("feedback.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();

            Toast.makeText(getApplicationContext(), "Feedback has been saved on your device", Toast.LENGTH_SHORT).show();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    /**
     * Read feedback from file
     * @return The feedback
     */
    private String readFromFile() {

        String ret = "";

        try {
            InputStream inputStream = openFileInput("feedback.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
}


