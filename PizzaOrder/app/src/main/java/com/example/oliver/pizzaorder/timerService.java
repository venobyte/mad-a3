/**
 * Project: MAD A3 - Pizza Order
 * Authors: Kelson Conyard, Josh Medeiros, Oliver Sousa, Aric Vogel
 * Date: 2016-04-15
 * Description: Timer service for Pizza Order.
 */

package com.example.oliver.pizzaorder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.app.PendingIntent;
import android.util.Log;
import android.widget.Toast;

/**
 * The timer service
 */
public class timerService extends Service {
    public timerService() {
    }

    CountDownTimer pizzaTimer = null;
    NotificationManager manager = null;
    BroadcastReceiver mScreenStateReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            //check to see if timer is finished
            if(checkCountdown()) {
                //toast that pizza is done, otherwise do nothing
                Toast toast = Toast.makeText(getApplicationContext(), "Pizza is finished", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    };

    /**
     * Creates the timer service
     */
    public void onCreate() {

        Intent notificationIntent = new Intent(this, pizzaCooked.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);


        int piFlag = PendingIntent.FLAG_UPDATE_CURRENT;
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, piFlag);

        CharSequence tickerText = "Pizza Timer";
        CharSequence contentTitle = "Pizza is Done!";
        CharSequence contentText = "Your Pizza is Done!";

        IntentFilter screenStateFilter = new IntentFilter();
        screenStateFilter.addAction(Intent.ACTION_SCREEN_ON);
        registerReceiver(mScreenStateReceiver, screenStateFilter);

        int icon = R.drawable.pizzaimage;

        final Notification notification = new Notification.Builder(this)
                .setSmallIcon(icon)
                .setTicker(tickerText)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build();

        manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);


        ConnectivityManager conManager = (ConnectivityManager)
                getSystemService(CONNECTIVITY_SERVICE);


        pizzaTimer = new CountDownTimer(30000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }
            @Override
            public void onFinish() {
                manager.notify(1, notification);
            }
        }.start();

    }

    /**
     * Handles startup of the service
     * @param intent The service intent
     * @param flags Flags for the service
     * @param startId A unique id for this service
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("PizzaOrder", "Service Started with id=" + startId);
        return START_STICKY;
    }

    /**
     * Handles binding of the service
     * @param intent The intent the service was bound with
     * @return An IBinder to call this service
     */
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handles destroying the service
     */
    @Override
    public void onDestroy() {
        //stopCountdown();
        Log.d("PizzaOrder", "Service Destroyed");
        unregisterReceiver(mScreenStateReceiver);
        manager.cancel(1);
    }

    /**
     * Checks if the countdown exists
     * @return Whether or not the countdown exists
     */
    public boolean checkCountdown() {
        if(pizzaTimer != null) {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Stops the countdown
     */
    private void stopCountdown(){
        if(pizzaTimer!=null){
            pizzaTimer.cancel();
        }
    }

}
